import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class Calculator {
  calculatePrice(price: number): number {
    return price * 1.2;
  }
}
