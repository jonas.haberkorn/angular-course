import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Highlight } from './directives/attribute/highlight';
import { UnderlineOnHover } from './directives/attribute/underline';
import { Loop } from './directives/structural/loop';
import { If } from './directives/structural/if';

import { Calculator } from './services/calculator.service';

@NgModule({
  declarations: [AppComponent, Highlight, UnderlineOnHover, Loop, If],
  imports: [BrowserModule, AppRoutingModule],
  providers: [Calculator],
  bootstrap: [AppComponent],
})
export class AppModule {}
