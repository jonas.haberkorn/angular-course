import { Directive, HostBinding, HostListener, Input } from '@angular/core';
import { Calculator } from '../../services/calculator.service';

@Directive({
  selector: '[highlight]',
})
export class Highlight {
  private id: number;
  private calculator: Calculator;

  @HostBinding('style.backgroundColor')
  @Input('bg-color')
  bgColor = 'yellow';

  @HostListener('click')
  onClick() {
    console.log("Hello! I'm " + this.id.toString());
    console.log(this.calculator.calculatePrice(100));
    this.bgColor = 'lightgreen';
  }

  constructor(calculator: Calculator) {
    this.id = Math.random();
    this.calculator = calculator;
  }
}
