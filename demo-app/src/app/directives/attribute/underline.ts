import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[underline-on-hover]',
})
export class UnderlineOnHover {
  constructor() {}

  @HostBinding('style.textDecoration')
  textDecoration: string = 'none';

  @HostListener('mouseenter')
  onMouseEnter() {
    this.textDecoration = 'underline';
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.textDecoration = 'none';
  }
}
