import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[loop]',
})
export class Loop implements OnInit {
  @Input('loop')
  numberOfInstances: number = 1;

  constructor(
    private template: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {
    for (let i = 0; i < this.numberOfInstances; i++) {
      this.viewContainer.createEmbeddedView(this.template, {
        number: i,
      });
    }
  }
}
