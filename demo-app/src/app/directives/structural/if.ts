import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[if]',
})
export class If implements OnInit {
  @Input('if')
  condition: boolean = false;

  constructor(
    private template: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {
    if (this.condition === true)
      this.viewContainer.createEmbeddedView(this.template);
  }
}
